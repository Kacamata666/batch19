// Soal No. 1 (Membuat kalimat)

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
console.log (word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh)

var space = " "
console.log (space)

// Soal No.2 Mengurai kalimat (Akses karakter dalam string)

var sentence = "I am going to be React Native Developer"; 

var firstword = sentence[0]
var secondword = sentence[2]+sentence[3]
var thirdword = sentence[5]+sentence[6]+sentence[7]+sentence[8]+sentence[9]
var fourthword = sentence[11]+sentence[12]
var fifthhword = sentence[14]+sentence[15]
var sixthword = sentence[17]+sentence[18]+sentence[19]+sentence[20]+sentence[21]
var seventhword = sentence[23]+sentence[24]+sentence[25]+sentence[26]+sentence[27]+sentence[28]
var eighthword = sentence[30]+sentence[31]+sentence[32]+sentence[33]+sentence[34]+sentence[35]+sentence[36]+sentence[37]+sentence[38]

console.log ('First Word:'+" "+firstword)
console.log ('Second Word:'+" "+secondword)
console.log ('Third Word:'+" "+thirdword)
console.log ('Fourth Word:'+" "+fourthword)
console.log ('Fifth Word:'+" "+fifthhword)
console.log ('Sixth Word:'+" "+sixthword)
console.log ('Seventh Word:'+" "+seventhword)
console.log ('Eight Word:'+" "+eighthword)

var space = " "
console.log (space)

//Soal No. 3 Mengurai Kalimat (Substring)

var sentence2 = 'wow JavaScript is so cool';

var firstword2 = sentence2.substring(0, 3); 
var secondword2 = sentence2.substring(4, 14);
var thirdword2 = sentence2.substring(15, 17);
var fourthword2 = sentence2.substring(18, 20);
var fifthword2 = sentence2.substring(21, 25);

console.log ('First Word:'+" "+(firstword2))
console.log ('Second Word:'+" "+(secondword2))
console.log ('Third Word:'+" "+(thirdword2))
console.log ('Fourth Word:'+" "+(fourthword2))
console.log ('Fifth Word:'+" "+(fifthword2))

var space = " "
console.log (space)

//Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String

var sentence3 = 'wow JavaScript is so cool';

var firstword3 = sentence3.substring(0, 3);
var secondword3 = sentence3.substring(4, 14);
var thirdword3 = sentence3.substring(15, 17);
var fourthword3 = sentence3.substring(18, 20);
var fifthword3 = sentence3.substring(21, 25);

var firstWordLength = firstword3.length
var secondWordLength = secondword3.length
var thirdWordLength = thirdword3.length
var fourthWordLength = fourthword3.length
var fifthWordLength = fifthword3.length

console.log('First Word: ' + firstword3 + ', with length: ' + firstWordLength);
console.log('Second Word: ' + secondword3 + ', with length: ' + secondWordLength);
console.log('Third Word: ' + thirdword3 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthword3 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthword3 + ', with length: ' + fifthWordLength);

