// 1 Looping 
// LOOPING PERTAMA

console.log('LOOPING PERTAMA')

let i = 0;
do {
  i += 2;
  console.log(i + ' - ' + 'I love coding' );
} while (i < 20);

console.log('==========================================')

console.log('LOOPING KEDUA')

console.log('==========================================')

let a = 22;
do {
  a -= 2;
  console.log(a + ' - ' + 'I will become a mobile developer' );
} while (a > 2);

console.log('==========================================')
// No. 2 Looping menggunakan for
console.log('No. 2 Looping menggunakan for')

console.log('==========================================')

for (var ba = 1; ba <= 20; ba += 1 ) {
  console.log( ba + ' - c ');
}



console.log('==========================================')
// 
console.log('No. 3 Membuat Persegi Panjang #')

console.log('==========================================')

for (var z = 1; z <= 4; z++ ) {
  console.log( "##########" );
}

console.log('==========================================')
// 
console.log('No. 4 Membuat Tangga #')

console.log('==========================================')

for (var z = 1; z <= 1; z++ ) {
  console.log( "#" );
}

for (var z = 1; z <= 1; z++ ) {
  console.log( "##" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "###" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "####" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "#####" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "######" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "#######" );
}

console.log('==========================================')
// 
console.log('No. 5 Membuat Papan Catur')

console.log('==========================================')

for (var z = 1; z <= 1; z++ ) {
  console.log( "# " +"# "+"# "+"# " );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( " #" +" #"+" #"+" #" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "# " +"# "+"# "+"# " );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( " #" +" #"+" #"+" #" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "# " +"# "+"# "+"# " );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( " #" +" #"+" #"+" #" );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( "# " +"# "+"# "+"# " );
}
for (var z = 1; z <= 1; z++ ) {
  console.log( " #" +" #"+" #"+" #" );
}