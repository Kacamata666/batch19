// No. 1 Tulislah sebuah function dengan nama teriak() 
// yang mengembalikan nilai “Halo Sanbers!” yang kemudian dapat ditampilkan di console.

function txtOut() {
    var txtOut = "Halo Sanbers!"
    return txtOut
} 
var teriak = txtOut ()
console.log(teriak)

console.log("=========================================================")

// No. 2 Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter yang di kirim.

console.log("=========================================================")

function kalikan (num1, num2) {
    return num1 * num2
} 
 
var hasilKali = kalikan(12, 4)
console.log(hasilKali)

console.log("=========================================================")

// No. 3 Function intro

console.log("=========================================================")

function introduce (name, age, address, hobby) {         
    return "Nama saya " + name + ", " + "umur saya " + age + ", " + 
    "alamat saya di " + address + ", " + "dan saya punya hobby yaitu " + hobby 
}
var name1 = "Agus"
var age1 = "30"
var address1 = "Jln. Malioboro, Yogyakarta"
var hobby1 = "Gaming" 
var kenalan = introduce(name1, age1, address1, hobby1)
console.log(kenalan) 

// Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 